package communication;

import concepts.LamportTime;

import java.io.Serializable;
import java.util.List;

/**
 * the class send to other process to comunicate, has lamport time to update the receiver time
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 4891657486165476354L;
    private String content;
    private LamportTime time;
    private String sourceAddress, destAddress;
    private List<CompeteProcess> processList = null;
    private ElectionToken electionToken;
    private long processId;

    public Message(String content, LamportTime time, String sourceAddress, String destAddress) {
        System.out.println("envoi de " + content);
        this.content = content;
        this.time = time;
        this.sourceAddress= sourceAddress;
        this.destAddress=destAddress;
    }

    public Message(List<CompeteProcess> processes, long processId, LamportTime time, String sourceAddress, String destAddress) {
        this.processId = processId;
        processList = processes;
        this.content = "Response";
        this.time = time;
        this.sourceAddress= sourceAddress;
        this.destAddress=destAddress;
        System.out.println("envoi de Responce of process " + processId);
    }

    public Message(List<CompeteProcess> processes, LamportTime time, String sourceAddress, String destAddress) {
        System.out.println("envoi de la liste de grosseur: " + processes.size());
        this.content = "Process List";
        this.time = time;
        this.sourceAddress= sourceAddress;
        this.destAddress=destAddress;
        processList = processes;
    }

    public Message(List<CompeteProcess> processes, LamportTime time, String sourceAddress, String destAddress, ElectionToken electionToken, boolean isVictory) {
        this.electionToken = electionToken;
        this.content = isVictory ? "Victory" : "Elect";
        this.time = time;
        this.sourceAddress= sourceAddress;
        this.destAddress=destAddress;
        processList = processes;
        System.out.println("envoi du message " + content + " et de la liste de grosseur: " + processes.size());
    }

    public List<CompeteProcess> getProcessList(){
        return processList;
    }

    public String getContent() {
        return content;
    }

    public LamportTime getTime() {
        return time;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public String getDestAddress() {
        return destAddress;
    }

    public ElectionToken getElectionToken() {
        return electionToken;
    }

    public long getProcessId() {
        return processId;
    }
}
