package communication;

import models.LamportAndProcessId;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LifeTester implements Runnable {

    private List<CompeteProcess> processList;
    private Process process;
    private List<LamportAndProcessId> lastCheckList;

    public void UpdateList(List<CompeteProcess> processList){
        this.processList = processList;
    }

    public LifeTester(List<CompeteProcess> processList, Process process){
        this.processList = processList;
        this.process = process;
        lastCheckList = new ArrayList<>();
        for (CompeteProcess cp: processList) {
            lastCheckList.add(new LamportAndProcessId(cp.getId(), cp.getTime().getTime()));
        }
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * verify that the other process has sent at least a message the last 10 seconds
     */
    @Override
    public void run() {
        while (true){
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (CompeteProcess competeProcess : processList) {
                boolean exist = false;
                for (LamportAndProcessId last : lastCheckList) {
                    if (competeProcess.getId() == last.Id) {
                        if (competeProcess.getTime().getTime() == last.time && !competeProcess.isLeadedBy(process.getID()) ){
                            competeProcess.setAlive(false);
                            process.startElectionFor(competeProcess.getId());
                            process.sendListOfProcess();
                        }
                        last.time = competeProcess.getTime().getTime();
                        exist = true;
                        break;
                    }
                }
                if (!exist){
                    lastCheckList.add(new LamportAndProcessId(competeProcess.getId(), competeProcess.getTime().getTime()));
                }
            }
        }
    }
}
