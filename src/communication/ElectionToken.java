package communication;

import java.io.Serializable;

public class ElectionToken  implements Serializable {
    private static final long serialVersionUID = 165658899994563169L;
    private final long currentProcessId;
    private final long deadProcessId;

    public ElectionToken(long currentProcessId, long deadProcessId){

        this.currentProcessId = currentProcessId;
        this.deadProcessId = deadProcessId;
    }

    public long getCurrentProcessId() {
        return currentProcessId;
    }

    public long getDeadProcessId() {
        return deadProcessId;
    }
}
