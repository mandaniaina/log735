package communication;

import concepts.LamportTime;
import models.Actions;
import models.Business;

import java.io.Serializable;
import java.util.List;

/**
 * contains the shared information across the network
 */
public class CompeteProcess implements Serializable {
    private static final long serialVersionUID = 165413574351535869L;
    private long id;
    private String address;
    private Business ownedBusiness;
    private LamportTime lamportTime;
    public List<Actions> ownedActions;
    public double money;
    private long leadedBy;

    public CompeteProcess (long id, String address) {
        money = 0;
        this.id = id;
        leadedBy = id;
        this.address = address;
        ownedBusiness = new Business();
        lamportTime = new LamportTime();
    }

    public boolean isMoreUpToDateThan(CompeteProcess other) {
        return lamportTime.isMoreUpToDateThan(other.lamportTime);
    }


    public void setAlive(boolean isAlive) {
        ownedBusiness.setOwnerAlive(isAlive);
    }

    public Business getBusiness(){
        return ownedBusiness;
    }

    public boolean isAlive() {
        return ownedBusiness.isAlive();
    }

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public LamportTime getTime() {
        return lamportTime;
    }

    public boolean isLeadedBy(long id) {
        return leadedBy == id;
    }

    public void setLeadedBy(long processID) {
        leadedBy = processID;
    }
}
