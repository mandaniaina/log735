package communication;//Timestamps need to be changed
import concepts.LamportTime;
import models.Actions;
import models.Business;
import vues.Accueil;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Process {
    private long ID;
    private List<CompeteProcess> processList;
    private ProcessState state;
    private String address;
    private DatagramChannel inputchannel;
    private DatagramChannel outputchannel;
    private String[] queue;
    private Selector selector;
    private List<Message> sendingDataTracking;
    private List<Message> receivedDataTracking;
    private final int udpPort = 9999;
    private Thread CommunicationThread;
    private Thread OutputThread;
    private static final int maxMessageSize = 10000;
    private Accueil accueil;
    private LifeTester lifeTester;
    private Bully bully;

    //Getters and Setters

    private Actions getActionsForBusiness(long id){
        for (Actions actions: getOwnCompeteProcess().ownedActions) {
            if (actions.isCorrectId(id))
            {return actions;}
        }
        Actions actions = new Actions(id, false);
        getOwnCompeteProcess().ownedActions.add(actions);
        return actions;
    }

    public List<CompeteProcess> getLeadedProcess(){
        List<CompeteProcess>  value = new ArrayList<>();
        for (CompeteProcess competeProcess : processList) {
            if (competeProcess.isLeadedBy(getID())){
                value.add(competeProcess);
            }
        }
        return value;
    }

    public long getID() {
        return ID;
    }

    Selector getSelector() {
        return selector;
    }

    String getAddress() {
        return address;
    }

    ProcessState getState() {
        return state;
    }

    public List<CompeteProcess> getProcessList() {
        return processList;
    }

    LamportTime getTime() {
        return getOwnCompeteProcess().getTime();
    }

    List<Message> getSendingDataTracking() {
        return sendingDataTracking;
    }

    void setSendingDataTracking(List<Message> sendingDataTracking) {
        this.sendingDataTracking = sendingDataTracking;
    }

    public void sendMessage(Message message){
        getTime().incrementTime();
        byte[] bytes = new byte[0];
        try {
            bytes = convertToBytes(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.clear();
        buffer.put(bytes);
        buffer.flip();
        try {
            synchronized (outputchannel) {
                outputchannel.send(buffer, new InetSocketAddress(message.getDestAddress().isEmpty() ? "255.255.255.255": message.getDestAddress(), udpPort));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendListOfProcess(){
        sendMessage(new Message(processList, getTime(), address, ""));
    }

    void setSelector(Selector selector) {
        this.selector = selector;
    }

    List<Message> getReceivedDataTracking() {
        return receivedDataTracking;
    }

    void setReceivedDataTracking(List<Message> receivedDataTracking) {
        this.receivedDataTracking = receivedDataTracking;
    }

    String[] getQueue() {
        return queue;
    }

    void setQueue(String[] queue) {
        this.queue = queue;
    }

    //Constructor
    public Process(Accueil accueil) {
        receivedDataTracking = new ArrayList<>();
        this.accueil = accueil;
        sendingDataTracking = new ArrayList<>();
        state = new ProcessState();
        processList = new ArrayList<>();
        try {
            address = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            System.out.println("No address found");
        }
        //on assume que son ip est unique et ne change pas lors de la reconnection
        ID = Long.parseLong(address.replaceAll("\\D+",""));
        queue = new String[1];
        lifeTester = new LifeTester(getProcessList(), this);
        bully = new Bully(this);
    }

    public CompeteProcess getOwnCompeteProcess(){
        for (CompeteProcess c: processList) {
            if (c.getId() == ID)
                return c;
        }
        return null;
    }

    public void startElectionFor(long id){
        bully.StartElectionsFor(id);
    }

    public CompeteProcess getCompeteProcess(long id){
        for (CompeteProcess c: processList) {
            if (c.getId() == id)
                return c;
        }
        return null;
    }

    //Initial phase
    public void init() throws IOException {
        //Open a DatagramChannel for listening to income messages
        inputchannel = DatagramChannel.open();
        inputchannel.socket().bind(new InetSocketAddress(9999));
        inputchannel.socket().setBroadcast(true);
        inputchannel.configureBlocking(false);
        selector = Selector.open();
        outputchannel=DatagramChannel.open();
        outputchannel.socket().setBroadcast(true);
        outputchannel.configureBlocking(false);
        processList.add(new CompeteProcess(this.ID, this.address));
        int i=3;
        while (i>0) {
            helloBroadcast();
            //attendre la réponse
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if( readProcessList()) break;
            i--;
        }
        getTime().boostTime();
        outputchannel.register(selector, SelectionKey.OP_READ);
        if (getOwnCompeteProcess().ownedActions == null){
            getOwnCompeteProcess().ownedActions = new ArrayList<>();
            getOwnCompeteProcess().ownedActions.add(new Actions(this.ID, true));
        }
    }

    //Broadcast a Hello Msg to receive the process list
    private void helloBroadcast() {
        String content = ID + "/Hello";
        getTime().incrementTime();
        Message message = new Message(content, getTime(), address, "");
        sendMessage(message);
    }

    //Receive process list
    private boolean readProcessList() throws IOException{
        ByteBuffer buf = ByteBuffer.allocate(maxMessageSize);
        buf.clear();
        inputchannel.configureBlocking(false);
        inputchannel.receive(buf);
        if (buf.limit() != buf.remaining()) {
            byte[] data = new byte[buf.limit() - buf.remaining()];
            buf.flip();
            buf.get(data);
            try {
                Object fromBytes = convertFromBytes(data);
                if (!(fromBytes instanceof Message)){
                    System.out.println("erreur ce n'est pas un message");
                    return false;
                }
                Message recieved = (Message)fromBytes;

                //ignorer ses propres messages
                if (recieved.getSourceAddress().equals(address)){
                    return readProcessList();
                }
                if (recieved.getProcessList() == null){
                    System.out.println("erreur aucune liste reçu");
                    return false;
                }

                System.out.println("liste reçu");
                updateCompeteProcessList(recieved.getProcessList());

            } catch (ClassNotFoundException e) {
                inputchannel.configureBlocking(false);
                return false;
            }
            System.out.println("received: " + new String(data));

            return true;
        }
        inputchannel.configureBlocking(false);
        System.out.println("Rien Recu");
        return false;
    }

    private void updateCompeteProcessList(List<CompeteProcess> receivedList){
        synchronized (processList){
            for (CompeteProcess received : receivedList){
                //si je ne l'ai pas ajoute le, sinon, si celui que j'ai a un plus petit temps, remplace le
                if (getCompeteProcess(received.getId()) == null){
                    processList.add(received);
                }else if (!getCompeteProcess(received.getId()).isMoreUpToDateThan(received)) {
                    processList.remove(getCompeteProcess(received.getId()));
                    processList.add(received);
                }
            }
        }
        CompeteProcess cp = getOwnCompeteProcess();
        if (!cp.isLeadedBy(getID())){
            cp.setLeadedBy(getID());
            cp.getTime().boostTime();
        }
        lifeTester.UpdateList(processList);
        accueil.updateInterface();
    }

    public void start(){
        if (CommunicationThread == null) {
            CommunicationThread = new Thread(new Runnable() {
                public void run () {
                    ByteBuffer buffer = ByteBuffer.allocate(maxMessageSize);
                    while (true) {
                        getOwnCompeteProcess().setAlive(true);
                        try {
                            inputchannel.configureBlocking(true);
                            inputchannel.receive(buffer);
                            if (buffer.remaining() != buffer.limit()) {
                                byte[] data = new byte[buffer.limit() - buffer.remaining()];
                                buffer.flip();
                                buffer.get(data);
                                Message message = (Message)convertFromBytes(data);

                                //ignorer ses propres messages
                                if (message.getSourceAddress().equals(address)){
                                    buffer.clear();
                                    continue;
                                }
                                String content = message.getContent();
                                System.out.println("Message reçu: " + content + ((message.getProcessList() == null) ? "" : "et une liste de grosseur: " + message.getProcessList().size()));

                                if (content.contains("Hello")) {
                                    buffer = ByteBuffer.allocate(maxMessageSize);
                                    data = convertToBytes(new Message(processList, getTime(), address, message.getSourceAddress()));
                                    buffer.put(data);
                                    buffer.flip();
                                    synchronized (outputchannel) {
                                        outputchannel.send(buffer, new InetSocketAddress(message.getSourceAddress(), udpPort));
                                    }
                                } else if (content.equals("Request")) {
                                    System.out.println("999999999999999999999999");
                                    synchronized (this) {
                                        RichardAgrawala.ReceivedReqHandler(Process.this, message);
                                    }
                                } else if (content.equals("Response")) {
                                    updateCompeteProcessList(message.getProcessList());
                                    synchronized (receivedDataTracking) {
                                        receivedDataTracking.add(message);
                                    }
                                } else if (content.equals("Process List")) {
                                    updateCompeteProcessList(message.getProcessList());
                                } else if (content.equals("Elect")) {
                                    updateCompeteProcessList(message.getProcessList());
                                    if (message.getElectionToken().getCurrentProcessId() > getID() || message.getElectionToken().getDeadProcessId() == getID()){
                                        sendMessage(new Message(processList, getTime(), address, "", message.getElectionToken(), true));
                                    }else {
                                        // start election if received lower or same id as elect
                                        bully.StartElectionsFor(message.getElectionToken().getCurrentProcessId());
                                    }
                                } else if (content.equals("Victory")) {
                                    if (message.getElectionToken().getCurrentProcessId() > getID() && message.getElectionToken().getDeadProcessId() != getID()){
                                        bully.receiveVictory(message.getElectionToken());
                                    }else {
                                        // start election if received lower or same id as victory
                                        bully.StartElectionsFor(message.getElectionToken().getCurrentProcessId());
                                    }
                                }
                                synchronized (getTime()) {
                                    getTime().update(message.getTime());
                                }
                                buffer.clear();
                            }
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            CommunicationThread.start();
        }
        if (OutputThread == null){
            OutputThread = new Thread(() -> {
                Message message;
                ByteBuffer buffer = ByteBuffer.allocate(maxMessageSize);
                while (true){
                    synchronized (sendingDataTracking) {
                        Iterator<Message> iterator = sendingDataTracking.iterator();

                        while (iterator.hasNext()) {
                            message = iterator.next();
                            iterator.remove();
                            byte[] bytes = new byte[0];
                            try {
                                bytes = convertToBytes(message);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            buffer.clear();
                            buffer.put(bytes);
                            buffer.flip();
                            try {
                                outputchannel.send(buffer, new InetSocketAddress(message.getDestAddress(), udpPort));
                            } catch (IOException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    buffer.clear();
                }
            });
            OutputThread.start();
        }
    }

    private static byte[] convertToBytes(Object message) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(message);
            return bos.toByteArray();
        }
    }

    private static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return  in.readObject();
        }
    }

    //region operations
    public boolean buyAction(int numberOfAction, long processId){
        RichardAgrawala.Enter_CS(this);
        getTime().incrementTime();
        Business business = getCompeteProcess(processId).getBusiness();
        if (business.canBuyAction(numberOfAction, getOwnCompeteProcess().money))
        {
            getOwnCompeteProcess().money -= business.buyAction(numberOfAction);
            getActionsForBusiness(processId).buy(numberOfAction);
            RichardAgrawala.Exit_CS(this);
            return true;
        }
        RichardAgrawala.Exit_CS(this);
        return false;
    }

    public boolean sellAction(int numberOfAction, long processId){
        getTime().incrementTime();
        Business business = getCompeteProcess(processId).getBusiness();
        Actions actionsForBusiness = getActionsForBusiness(processId);
        if (actionsForBusiness.canSell(numberOfAction))
        {
            getOwnCompeteProcess().money += business.sellAction(numberOfAction);
            actionsForBusiness.sell(numberOfAction);
            sendListOfProcess();
            return true;
        }
        return false;
    }

    public boolean invest(double moneyToInvest){
        getTime().incrementTime();
        if (moneyToInvest <= getOwnCompeteProcess().money){
            getOwnCompeteProcess().money -= moneyToInvest;
            getOwnCompeteProcess().getBusiness().invest(moneyToInvest);
            sendListOfProcess();
            return true;
        }
        return false;
    }

    public int getNbActionsForProcess(long id){
        getTime().incrementTime();
        return getActionsForBusiness(id).getNumberPossessed();
    }

    public int getNbActionsForMyProcess(){
        getTime().incrementTime();
        return getActionsForBusiness(ID).getNumberPossessed();
    }

    public int getNbAvalibleActionsForProcess(long id){
        getTime().incrementTime();
        return getCompeteProcess(id).getBusiness().getAvalibleActions();
    }

    public double getValueOfActionOfProcess(long processId){
        getTime().incrementTime();
        return getCompeteProcess(processId).getBusiness().getActionValue();
    }

    public double getValueOfOwnAction(){
        getTime().incrementTime();
        return getOwnCompeteProcess().getBusiness().getActionValue();
    }

    public double getValueForOwnBusiness(long processId){
        getTime().incrementTime();
        return getCompeteProcess(processId).getBusiness().getValue();
    }

    public double getValueForOwnBusiness(){
        getTime().incrementTime();
        return getOwnCompeteProcess().getBusiness().getValue();
    }
    public double getMoney(){
        getTime().incrementTime();
        return getOwnCompeteProcess().money;
    }

    public double getInvestedMoney(long id){
        getTime().incrementTime();
        return getCompeteProcess(id).getBusiness().getInvestedMoney();
    }

    public double getInvestedMoney(){
        getTime().incrementTime();
        return getOwnCompeteProcess().getBusiness().getInvestedMoney();
    }

    public void updateActionValues(){
        for (CompeteProcess competeProcess : getLeadedProcess()) {
            competeProcess.getBusiness().updateValue();
            competeProcess.getTime().incrementTime();
        }
        sendListOfProcess();
    }

    //endregion
}
