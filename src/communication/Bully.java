package communication;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Bully implements Runnable{

    private Process process;
    private List<ElectionToken> victoryElectionTokensReceived;
    private long ID;

    public Bully(Process process){
        this.process = process;
    }

    public void StartElectionsFor(long processId){
        process.sendMessage(new Message(process.getProcessList(), process.getTime(), process.getAddress(), "", new ElectionToken(process.getID(), processId), false));
        ID = processId;
        victoryElectionTokensReceived = new ArrayList<>();
        Thread t = new Thread(this);
        t.start();
    }

    public void receiveVictory(ElectionToken electionToken){
        victoryElectionTokensReceived.add(electionToken);
    }

    public void removeTokenFor(long Id){
        for (ElectionToken token: victoryElectionTokensReceived) {
            if (token.getDeadProcessId() == Id){
                victoryElectionTokensReceived.remove(token);
            }
        }
    }

    public boolean isVictoriusFor(long Id){
        for (ElectionToken token: victoryElectionTokensReceived) {
            if (token.getDeadProcessId() == Id){
                return false;
            }
        }
        return true;
    }

    @Override
    public void run() {
        long id = ID;
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (isVictoriusFor(id)){
            process.getCompeteProcess(id).setLeadedBy(process.getID());
            System.out.println("Win Elections for " + id);
        }
        removeTokenFor(id);
    }
}
