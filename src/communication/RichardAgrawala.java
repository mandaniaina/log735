package communication;

import concepts.LamportTime;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class RichardAgrawala {
    static List<Long> ResponseList;
    private static final int waitBeforeRetry = 5;

    public static void Enter_CS(Process process) {
        try {
            ResponseList = new ArrayList<>();
            //retry if new process/leader or failed communication
            do{
                synchronized (process) {
                    process.getState().Wanted();
                    for (CompeteProcess proc : process.getProcessList()) {
                        if (proc.getId() == process.getID())
                            continue;
                        LamportTime time = process.getTime();
                        Message message = new Message("Request", time, process.getAddress(), proc.getAddress());
                        process.sendMessage(message);
                    }
                }
                int cpt = 0;
                do {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (process) {
                        List<Message> Rdatatrack = process.getReceivedDataTracking();
                        while (!Rdatatrack.isEmpty()) {
                            Message message = Rdatatrack.get(0);
                            if (message.getContent().equals("Response")) {
                                Rdatatrack.remove(0);
                                if (!ResponseList.contains(message.getProcessId()))
                                    ResponseList.add(message.getProcessId());
                            }
                            process.setReceivedDataTracking(Rdatatrack);
                        }
                    }
                } while (ResponseList.size() != process.getProcessList().size() && cpt++ < waitBeforeRetry);
            } while (ResponseList.size() != process.getProcessList().size() - 1);
            process.getState().Held();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void Exit_CS(Process process) {
        synchronized (process) {
            process.getState().Released();
            String[] queue = process.getQueue();

            for (String aQueue : queue) {
                for (CompeteProcess c : process.getLeadedProcess()) {
                    Message message = new Message(process.getProcessList(), c.getId(), process.getTime(), process.getAddress(), aQueue);
                    process.sendMessage(message);
                }
            }
        }
    }

    /**
     * when a request to enter CS is received
     * @param process our process
     * @param message the received message
     */
    public static void ReceivedReqHandler(Process process , Message message) {
        LamportTime time = message.getTime();
        synchronized (process) {
            ProcessState state = process.getState();

            if (state.isHeld() || (state.isWanted() && time.getTime() > process.getTime().getTime())) {
                String[] queue = process.getQueue();
                queue[queue.length - 1] = message.getSourceAddress();
                process.setQueue(queue);
            }

            else{

                List<Message> datatrack = process.getSendingDataTracking();
                for (CompeteProcess p:process.getLeadedProcess()) {
                    Message toSendMessage = new Message(process.getProcessList(),
                            p.getId(),
                            process.getTime(),
                            process.getAddress(),
                            message.getSourceAddress());
                    datatrack.add(toSendMessage);
                }
                process.setSendingDataTracking(datatrack);
            }
        }
    }
}

