package communication;

/**
 * the state of the process for Ricard & Agrawala
 */
public class ProcessState {
    private boolean Released, Wanted, Held;

    public ProcessState() {
        this.Released = true;
        this.Wanted = false;
        this.Held = false;
    }

public void Wanted(){
        Released=false;
        Wanted=true;
        Held=false;
}

public void Held(){
        Released=false;
        Wanted=false;
        Held=true;
}

public void Released() {
        Released=true;
        Wanted=false;
        Held=false;
}

    public boolean isReleased() {
        return Released;
    }

    public void setReleased(boolean released) {
        Released = released;
    }

    public boolean isWanted() {
        return Wanted;
    }

    public void setWanted(boolean wanted) {
        Wanted = wanted;
    }

    public boolean isHeld() {
        return Held;
    }

    public void setHeld(boolean held) {
        Held = held;
    }

}
