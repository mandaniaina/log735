package controllers;

import communication.Process;
import vues.Accueil;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Controller implements Runnable {
    private Process process;
    private Accueil accueil;

    public Controller(Accueil accueil){
        this.accueil = accueil;
        process = new Process(accueil);
        try {
            process.init();
            process.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * the update loop
     */
    public void run() {
        while (true){
            try {
                TimeUnit.SECONDS.sleep(ThreadLocalRandom.current().nextInt(1, 3 + 1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            process.updateActionValues();
            accueil.updateInterface();
        }
    }

    public Process getProcess() {
        return process;
    }
}
