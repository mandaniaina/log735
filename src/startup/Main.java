package startup;

import vues.Accueil;

public class Main {
    /**
     * run me to start the project
     * @param args
     */
    public static void main(String[] args) {
        Accueil.start();
    }
}
