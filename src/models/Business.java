package models;

import concepts.RandomDouble;

import java.io.Serializable;

public class Business implements Serializable {
    private double value;
    private double investedMoney;
    private int avalibleActions;
    private int emmitedActions;
    private boolean ownerAlive;

    public Business(){
        value = 100000;
        investedMoney = 0;
        avalibleActions = 0;
        emmitedActions = 0;
        ownerAlive = true;
    }

    /**
     * simulate a chash when the owner disconnect and a recovery when the owner reconnect
     * @param alive
     */
    public void setOwnerAlive(boolean alive){
        if (alive && !ownerAlive){
            value *= 8;
        }
        if (!alive && ownerAlive){
            value /= 10;
        }
        ownerAlive = alive;
    }

    public boolean isAlive(){
        return ownerAlive;
    }

    public double getValue(){
        return value;
    }


    public boolean canBuyAction(int numberOfActions, double currentMoney){
        return numberOfActions <= avalibleActions && currentMoney >= numberOfActions * getActionValue();
    }

    public double buyAction(int numberOfAction){
        avalibleActions -= numberOfAction;
        return numberOfAction * getActionValue();
    }


    public double sellAction(int numberOfAction){
        avalibleActions  += numberOfAction;
        return numberOfAction * getActionValue();
    }

    public double getActionValue(){
        return value / 1000;
    }

    public void invest(double money){
        investedMoney += money;
    }

    public double getInvestedMoney(){
        return investedMoney;
    }

    public void updateValue(){
        if (ownerAlive){
            double min = -(value / 10);
            double max = (value / 100) * (6 + investedMoney/10000 + emmitedActions/(avalibleActions + 1));
            value = value + RandomDouble.getTwoDecimal(min, max);
        }
        else {
            double onePercentValue = value / 100;
            value = value + RandomDouble.getTwoDecimal(-onePercentValue , onePercentValue);
        }
    }

    public int getAvalibleActions() {
        return avalibleActions;
    }

    public void setAvalibleActions(int avalibleActions) {
        this.avalibleActions = avalibleActions;
    }

    public int getEmmitedActions() {
        return emmitedActions;
    }

    public void setEmmitedActions(int emmitedActions) {
        this.emmitedActions = emmitedActions;
    }
}
