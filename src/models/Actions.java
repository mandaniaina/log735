package models;

import java.io.Serializable;

/**
 * This class represent the owned actions of a certain business
 */
public class Actions implements Serializable {
    private static final long serialVersionUID = 165658899994563169L;
    private int numberPossessed;
    private long processId;

    public Actions(long processId, boolean isMyCompanyDebut){
        this.processId = processId;
        numberPossessed = isMyCompanyDebut ? 1000 : 0;
    }

    public boolean isCorrectId(long id){
        return id == processId;
    }

    public int getNumberPossessed() {
        return numberPossessed;
    }
    public void buy(int numberOfActions) {
        numberPossessed += numberOfActions;
    }
    public boolean canSell(int numberOfAction){
        return numberPossessed >= numberOfAction;
    }
    public void sell(int numberOfAction) {
        numberPossessed -= numberOfAction;
    }
}
