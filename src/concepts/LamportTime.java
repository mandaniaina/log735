package concepts;

import java.io.Serializable;

public class LamportTime implements Serializable {
    private static final long serialVersionUID = 1633342352436436369L;
    private long currentTime = 1;

    public long getTime(){
        return currentTime;
    }

    public boolean isMoreUpToDateThan(LamportTime otherTime){
        long otherTimeTime = otherTime.getTime();
        return otherTimeTime < currentTime;
    }

    public void update(LamportTime other){
        currentTime = Math.max(currentTime, other.currentTime) + 1;
    }

    public void incrementTime(){
        currentTime++;
    }
    //when restart to retake the process
    public void boostTime(){
        currentTime += 1000;
    }
}
